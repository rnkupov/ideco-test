export function filter(status, city) {
  return {
    type: 'FILTER',
    payload: {
      status: status,
      city: city
    }
  }
}
export function del(id) {
  return {
    type: 'DEL',
    payload: id
  }
}
export function edit(id) {
  return {
    type: 'EDIT',
    payload: id
  }
}
export function update(data) {
  return {
    type: 'UPDATE',
    payload: data
  }
}
export function add() {
  return {
    type: 'ADD',
    payload: {}
  }
}