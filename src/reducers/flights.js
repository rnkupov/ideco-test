import _ from 'lodash'

const initialState = {
  search: '',
  edit: undefined,
  data: [{
    id: 1,
    number: 777,
    arrival_city: 'Екатеринбург',
    departure_city: 'Москва',
    type: 'Airbus A310',
    time: '20:00 23.05.2017',
    cut_time: '',
    status: 3
  },{
    id: 2,
    number: 666,
    arrival_city: 'Екатеринбург',
    departure_city: 'Питер',
    type: 'Airbus A310',
    time: '20:00 23.05.2017',
    cut_time: '',
    status: 2
  },{
    id: 3,
    number: 555,
    arrival_city: 'Владивосток',
    departure_city: 'Москва',
    type: 'Airbus A310',
    time: '20:00 23.05.2017',
    cut_time: '',
    status: 1
  }]
}

export default function userstate(state = initialState, action) {
  switch (action.type) {
    case 'FILTER':
      var obj;
      if(action.payload.status || action.payload.city){
        obj = _.filter(
          initialState.data,
          function(item){
            var is_status
            var is_city

            if(action.payload.status){
              is_status = action.payload.status.indexOf(item.status) > -1
            }else{
              is_status = true
            }

            if(location.href.indexOf('departure') === -1){
              is_city = item.arrival_city.indexOf(action.payload.city) > -1
            }else{
              is_city = item.departure_city.indexOf(action.payload.city) > -1
            }

            return is_city && is_status
          }
        )
      }else{
        obj = initialState.data
      }

      return {
        ...state,
        data: obj
      }

    case 'DEL':
      if(!state.edit){
        return {
          ...state,
          data: _.pull(initialState.data, _.find(state.data, {id:action.payload}))
        }
      }else{
        return state
      }

    case 'EDIT':
      return {
        ...state,
        edit: action.payload
      }

    case 'UPDATE':
      var data = [];

      initialState.data.forEach(function(item){
        if(item.id === action.payload.id){
          data.push(action.payload)
        }else{
          data.push(item)
        }
      })

      initialState.data = data

      return {
        ...state,
        data: data,
        edit: undefined
      }

    case 'ADD':
      if(!state.edit){
        var id = initialState.data.length + 1

        initialState.data.push({
          id: id,
          number: '',
          arrival_city: '',
          departure_city: '',
          type: '',
          time: '',
          cut_time: '',
          status: ''
        })

        return {
          ...state,
          data: initialState.data,
          edit: id
        }
      }else{
        return state
      }

    default:
      return state;
  }
}