import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import * as flightsActions from '../../actions/FlightsActions'
import Flight from '../Flight'

var isAdmin = function(){
  return location.href.indexOf('admin') > -1
}

class Flights extends Component{
  componentDidMount = function() {
    this.props.flightsActions.filter(this.props.status, '')

    if(this.props.flights.edit){
      this.props.flightsActions.edit(undefined)
    }
  }

  clickAdd = function(){
    this.props.flightsActions.add()
  }

  onKeyUp = function() {
    var val = ReactDOM.findDOMNode(this.refs.search).value
    console.log(val)
    this.props.flightsActions.filter(this.props.status, val)
  }

  render() {
    var flights = this.props.flights;
    var actions = this.props.flightsActions;

    var newsTemplate = flights.data.map(function(flight) {
      return (
        <Flight
          item={flight}
          actions={actions}
          key={flight.id}
          edit={flights.edit}
        />
      )
    })

    return (
      <div>
        <input
          className='flight_input'
          defaultValue=''
          ref='search'
          placeholder='поиск по городу'
          onKeyUp={::this.onKeyUp}
        />
        <table className='flight'>
          <thead className='flight_head'>
            <tr className='flight_row'>
              <td className='flight_item'>номер</td>
              <td className='flight_item'>город вылета</td>
              <td className='flight_item'>город прилета</td>
              <td className='flight_item'>тип самолета</td>
              <td className='flight_item'>время</td>
              <td className='flight_item'>текущее время</td>
              <td className='flight_item'>статус</td>
              <td className='flight_item'></td>
            </tr>
          </thead>

          <tbody className='flight_body'>
            {newsTemplate}
          </tbody>
        </table>
        <div
          className={isAdmin()?'flight_add':'flight_add none'}
          onClick={::this.clickAdd}
        >
          Добавить
        </div>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    flights: state.flights
  }
}
function mapDispatchToProps(dispatch) {
  return {
    flightsActions: bindActionCreators(flightsActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Flights)