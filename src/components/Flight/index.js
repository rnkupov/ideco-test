import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'

var isAdmin = function(){
  return location.href.indexOf('admin') > -1
}
var status_all = [
  { value: 1, label: 'идет посадка' },
  { value: 2, label: 'вылетел' },
  { value: 3, label: 'приземлился' },
  { value: 4, label: 'задержан' }
];
export default class Flight extends Component{
  clickDelete = function() {
    this.props.actions.del(this.props.item.id)
  }

  clickEdit = function(e) {
    if(isAdmin() && !e.target.classList.contains('flight_del')){
      this.props.actions.edit(this.props.item.id)
    }
  }

  clickAdd = function() {
    var statusObj = _.find(
      status_all, {
        label:
        ReactDOM.findDOMNode(this.refs.status).value
      }
    )
    var data = {
      id: this.props.item.id,
      number: ReactDOM.findDOMNode(this.refs.number).value,
      arrival_city: ReactDOM.findDOMNode(this.refs.arrival_city).value,
      departure_city: ReactDOM.findDOMNode(this.refs.departure_city).value,
      type: ReactDOM.findDOMNode(this.refs.type).value,
      time: ReactDOM.findDOMNode(this.refs.time).value,
      cut_time: ReactDOM.findDOMNode(this.refs.cut_time).value,
      status: statusObj?statusObj.value:''
    }

    this.props.actions.update(data)
  }

  onChange = function() {
    ReactDOM.findDOMNode(this.refs.number).value
    ReactDOM.findDOMNode(this.refs.arrival_city).value
    ReactDOM.findDOMNode(this.refs.departure_city).value
    ReactDOM.findDOMNode(this.refs.type).value
    ReactDOM.findDOMNode(this.refs.time).value
    ReactDOM.findDOMNode(this.refs.cut_time).value
    ReactDOM.findDOMNode(this.refs.status).value
  }

  render() {
    var id = this.props.item.id;
    var number = this.props.item.number;
    var arrival_city = this.props.item.arrival_city;
    var departure_city = this.props.item.departure_city;
    var type = this.props.item.type;
    var time = this.props.item.time;
    var cut_time = this.props.item.cut_time;
    var statusObj = _.find(status_all, {value: this.props.item.status})
    var status = statusObj?statusObj.label:''

    if(this.props.edit === id){
      return (
        <tr className='flight_row'>
          <td className='flight_item'>
            <input
              className='flight_input'
              defaultValue={number}
              ref='number'
              placeholder='номер'
            />
          </td>
          <td className='flight_item'>
            <input
              className='flight_input'
              defaultValue={arrival_city}
              ref='arrival_city'
              placeholder='город вылета'
            />
          </td>
          <td className='flight_item'>
            <input
              className='flight_input'
              defaultValue={departure_city}
              ref='departure_city'
              placeholder='город прилета'
            />
          </td>
          <td className='flight_item'>
            <input
              className='flight_input'
              defaultValue={type}
              ref='type'
              placeholder='тип самолета'
            />
          </td>
          <td className='flight_item'>
            <input
              className='flight_input'
              defaultValue={time}
              ref='time'
              placeholder='время'
            />
          </td>
          <td className='flight_item'>
            <input
              className='flight_input'
              defaultValue={cut_time}
              ref='cut_time'
              placeholder='текущее время'
            />
          </td>
          <td className='flight_item'>
            <input
              className='flight_input'
              defaultValue={status}
              ref='status'
              placeholder='статус'
            />
          </td>
          <td className='flight_item'>
            <p className='flight_ok' onClick={::this.clickAdd}/>
          </td>
        </tr>
      )
    }else{
      return (
        <tr className='flight_row' onClick={::this.clickEdit}>
          <td className='flight_item'><p>{number}</p></td>
          <td className='flight_item'><p>{arrival_city}</p></td>
          <td className='flight_item'><p>{departure_city}</p></td>
          <td className='flight_item'><p>{type}</p></td>
          <td className='flight_item'><p>{time}</p></td>
          <td className='flight_item'><p>{cut_time}</p></td>
          <td className='flight_item'><p>{status}</p></td>
          <td className='flight_item'>
            <p
              className={isAdmin()?'flight_del':'flight_del none'}
              onClick={::this.clickDelete}
            />
          </td>
        </tr>
      )
    }
  }
}