import React, { Component } from 'react'
import { Link } from 'react-router'

export default class App extends Component {
  render() {
    return (
      <div className='container'>
        <div className='menu'>
          <p className='menu_item'><Link to='/'>Прилет</Link></p>
          <p className='menu_item'><Link to='/departure'>Вылет</Link></p>
          <p className='menu_item'><Link to='/admin'>Админ</Link></p>
        </div>
        <div className='content'>
          {this.props.children}
        </div>
      </div>
    )
  }
}