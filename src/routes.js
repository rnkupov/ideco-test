import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from './containers/App'
import Admin from './components/Admin'
import Departure from './components/Departure'
import Arrival from './components/Arrival'

export const routes = (
  <Route path='/' component={App}>
    <IndexRoute component={Arrival} />
    <Route path='departure' component={Departure} />
    <Route path='admin' component={Admin} />
  </Route>
)